import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ScanPage} from "../scan/scan";
import { ProgrammePage} from "../programme/programme";
import { StatsPage} from "../stats/stats";


@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    constructor(public navCtrl: NavController) {

    }

    public gotoScanPage() {
        this.navCtrl.push(ScanPage);
    }
    public gotoProgrammePage() {
        this.navCtrl.push(ProgrammePage);
    }
    public gotoStatsPage() {
        this.navCtrl.push(StatsPage);
    }

}