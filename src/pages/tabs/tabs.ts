import { Component } from '@angular/core';

import { ProgrammePage } from '../programme/programme';
import { ScanPage } from '../scan/scan';
import { HomePage } from '../home/home';
import { StatsPage } from '../stats/stats';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ScanPage;
  tab3Root = ProgrammePage;
  tab4Root = StatsPage;

  constructor() {

  }
}
