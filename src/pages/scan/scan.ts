import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { BarcodeScanner ,BarcodeScannerOptions } from '@ionic-native/barcode-scanner';

import { DecryptProvider } from "../../providers/decrypt/decrypt";

@Component({
    selector: 'page-scan',
    templateUrl: 'scan.html'
})
export class ScanPage {

    public firstname : string;      // ticket data
    public lastname : string;       // ...
    public email : string;
    // public phone : string;
    public nb_adults_sat : string;
    public nb_kids_sat : string;
    public nb_adults_sun : string;
    public nb_kids_sun : string;
    public security : string;       // encrypted security phrase

// example
//"{"firstname":"La grange","lastname":"Arts",
// "email":"asso.grangeauxarts@gmail.com",
// "nb_adults_sat":"2","nb_kids_sat":0,"nb_adults_sun":"2","nb_kids_sun":0,
// "security":"UJtAs2iNg9co2n2aOqwsCgwXWmRlFHSvSbWuYg7vRN9LcyDZRiD\/wruuKoJsQTB0OGu1+wSstge2ugxLrySljQ=="}"

    public decryptedData: string;   // decoded security
    public verification : boolean;  // verified ticket
    options :BarcodeScannerOptions;

    public errorMsg : string = "";  // error msg to display

    constructor(public navCtrl: NavController,
                private barcodeScanner: BarcodeScanner,
                private decrypt: DecryptProvider) {
    }

    public scan(){
        var stringData: string;
        this.init_variables();
        this.options = {
            prompt : "Scaner le QRcode parallèlement à l'appareil "
        }
        this.barcodeScanner.scan(this.options).then((barcodeData) => {

            if (barcodeData.text) {
                stringData = barcodeData.text;
                var objData;
                try {
                    objData = JSON.parse(stringData);

                    this.firstname = objData.firstname;
                    this.lastname = objData.lastname;
                    this.email = objData.email;
                    // this.phone = objData.phone;
                    this.nb_adults_sat = objData.nb_adults_sat;
                    this.nb_kids_sat = objData.nb_kids_sat;
                    this.nb_adults_sun = objData.nb_adults_sun;
                    this.nb_kids_sun = objData.nb_kids_sun;
                    this.security = objData.security;
                    console.log('no error in scan');
                }
                catch(err) {
                    console.log('catch 1 : ' + err);
                    this.errorMsg = err;
                }

                console.log('security string: '+ this.security);
                var response = this.decrypt.decrypt(this.security);

                if (!response.error_state) {
                    console.log('no error in scan');
                    this.decryptedData = response.msg;
                    console.log('decrypted: '+ this.decryptedData);

                    if (this.decryptedData ==
                            this.email + this.firstname + this.lastname
                            + this.nb_adults_sat + this.nb_kids_sat
                            + this.nb_adults_sun + this.nb_kids_sun
                    ) {
                        this.verification = true;
                    } else {
                        this.verification = false;
                    }
                } else {
                    console.log('catch 2 : PB');
                    this.errorMsg="Données illisibles " + response.error_state;
                }

            } else {
                console.log('catch 3 : PB');
                this.errorMsg="Données illisibles " + response.error_state;;
            }
        }, (err) => {
            console.log('catch 4 : ' + err);
            this.errorMsg= "Scan Error occured : " + err;
        });
    }

    public init_variables(){
        console.log('INIT');
        this.firstname = '';
        this.lastname = '';
        this.email = '';
        // this.phone = '';
        this.nb_adults_sat = '';
        this.nb_kids_sat = '';
        this.nb_adults_sun = '';
        this.nb_kids_sun = '';
        this.security = '';
        this.verification = false;
        this.errorMsg ='';
    }
}
