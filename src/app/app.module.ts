import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ScanPage} from "../pages/scan/scan";
import { ProgrammePage} from "../pages/programme/programme";
import { StatsPage} from "../pages/stats/stats";

import { TabsPage } from '../pages/tabs/tabs';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { DecryptProvider } from '../providers/decrypt/decrypt';
import { SecurityProvider } from '../providers/security/security';

@NgModule({
    declarations: [
        MyApp,
        StatsPage,
        ProgrammePage,
        HomePage,
        TabsPage,
        ScanPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        StatsPage,
        ProgrammePage,
        HomePage,
        TabsPage,
        ScanPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        BarcodeScanner,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
    DecryptProvider,
    SecurityProvider,
    SecurityProvider
    ]
})
export class AppModule {}
