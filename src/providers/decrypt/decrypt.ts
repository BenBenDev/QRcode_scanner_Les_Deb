// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SecurityProvider } from '../security/security';

//import * as CryptoJS from 'crypto-js';
import CryptoJS from 'crypto-js';
//import {toBase64String} from "@angular/compiler/src/output/source_map";

@Injectable()
export class DecryptProvider {

    private error_state :boolean = false;

    constructor( // public http: HttpClient,
        private securityKey: SecurityProvider) {
        console.log('Hello DecryptProvider Provider *****************************');
    }


    public decrypt(encryptedText: string){

        //  H E L P
        //  https://www.davidebarranca.com/2012/10/crypto-js-tutorial-cryptography-for-dummies/

        // example
        // encryptedText = "EATki0RiDXVMQKot3+aobG7BEgcavsKdl20jMXBN+cSs6HnNKAbAP4MNiKraDhIL"

        // https://stackoverflow.com/questions/14958103/how-to-decrypt-message-with-cryptojs-aes-i-have-a-working-ruby-example
        // https://gist.github.com/alfredfrancis/8891bb3c713f512a7ca40ba902942ee4

        // !!!!!!!!!!                   !!!!!!!!!!
        // !!!!!!!!!!       D E B U G
        // !!!!!!!!!!                   !!!!!!!!!!
        var debugMode = true;

        // Decrypt
        var key =  this.securityKey.key;
        var iv =  key.substring(0,16);
        if (debugMode) {
            console.log('key :' + key);
            console.log('iv : ' + iv);
            console.log('encrypted received: ' + encryptedText);
        }

        var message;
        try {
            // Decrypt...
            if (debugMode) {console.log(CryptoJS.enc.Base64.parse(encryptedText));}

            var plaintextArray = CryptoJS.AES.decrypt(encryptedText,key, { iv: iv });
            if (debugMode) {console.log('plaintextArray : ' + plaintextArray);}
            message=plaintextArray.toString(CryptoJS.enc.Utf8);

            // console.log('plaintextArray2 : ' + CryptoJS.enc.Utf8.stringify(CryptoJS.enc.Base64.parse(plaintextArray)));
            // console.log('plaintextArray3 : ' +CryptoJS.AES.decrypt(encryptedText,key, { iv: iv }));
            if (debugMode) {console.log('message plain text : ' + message);}
            this.error_state = false;
        }
        catch(err) {
            this.error_state = true;
            message = err;
        }

        return {
            "error_state" : this.error_state,
            "msg" : message
        }

    }
    public  hex2ascii(hexx: string) {
        var hex = hexx.toString();//force conversion
        var str = '';
        for (var i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2)
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
        return str;
    }


}
